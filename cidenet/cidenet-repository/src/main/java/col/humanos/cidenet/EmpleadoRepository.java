package col.humanos.cidenet;

import col.humanos.cidenet.custom.EmpleadoRepositoryCustom;
import col.humanos.cidenet.model.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, BigInteger>, EmpleadoRepositoryCustom {

  @Query("SELECT emp FROM Empleado emp  WHERE emp.id = :id")
  List<Empleado> findById(@Param("id") BigInteger id);

}
