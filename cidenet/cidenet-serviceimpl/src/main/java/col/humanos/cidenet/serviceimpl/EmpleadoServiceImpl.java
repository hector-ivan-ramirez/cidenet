package col.humanos.cidenet.serviceimpl;

import col.humanos.cidenet.EmpleadoRepository;
import col.humanos.cidenet.model.Empleado;
import col.humanos.cidenet.service.EmpleadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class EmpleadoServiceImpl implements EmpleadoService {
  private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

  private EmpleadoRepository empleadoRepository;

  @Autowired
  public EmpleadoServiceImpl(EmpleadoRepository empleadoRepository) {
    this.empleadoRepository=empleadoRepository;
  }

  @Override
  public List<Empleado> busquedaAvanzada() {
    return empleadoRepository.findAll();
  }

  @Override
  public boolean guardarEmpleado(Empleado empleado) {
    boolean status=false;
    try {
      empleadoRepository.save(empleado);
      status=true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return status;
  }

  @Override
  public boolean borrarEmpleado(Empleado empleado) {
    boolean status=false;
    try {
      empleadoRepository.delete(empleado);
      status=true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return status;
  }

  @Override
  public List<Empleado> getEmpleadoByID(Empleado empleado) {
    return empleadoRepository.findById(empleado.getId());
  }
}
