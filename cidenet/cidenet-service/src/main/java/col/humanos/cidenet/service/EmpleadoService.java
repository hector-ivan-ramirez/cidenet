package col.humanos.cidenet.service;

import col.humanos.cidenet.model.Empleado;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional(readOnly = true)
public interface EmpleadoService {

  List<Empleado> busquedaAvanzada();

  boolean guardarEmpleado(Empleado empleado);

  boolean borrarEmpleado(Empleado empleado);

  List<Empleado> getEmpleadoByID(Empleado empleado);

}
