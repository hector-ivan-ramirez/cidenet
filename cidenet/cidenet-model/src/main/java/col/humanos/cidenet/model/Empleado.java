package col.humanos.cidenet.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "empleado")
public class Empleado implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "empleado_seq")
  @SequenceGenerator(name="empleado_seq", sequenceName = "empleado_id_seq",allocationSize=1)
  @Column(name = "id", nullable = false)
  private BigInteger id;


  @Column(name = "primer_apellido", nullable = false, length = 20)
  private String primerApellido;

  @Column(name = "segundo_apellido",nullable = false, length = 20)
  private String segundoApellido;

  @Column(name = "primer_nombre",nullable = false, length = 20)
  private String primerNombre;

  @Column(name = "otro_nombre",length = 20)
  private String otroNombre;

  @Column(name = "pais_empleo",nullable = false, length = 10)
  private String paisEmpleo;

  @Column(name = "tipo_identificacion",nullable = false, length = 5)
  private String tipoIdentificacion;

  @Column(name = "nro_identificacion",nullable = false, length = 20)
  private String nroIdentificacion;

  @Column(name = "correo_electronico",length = 100)
  private String correoElectronico;

  @Column(name = "fecha_ingreso",nullable = false)
  private Date fechaIngreso;

  @Column(name = "area",length = 10)
  private String area;

  @Column(name = "estado",nullable = false,length = 1)
  private String estado;

  @Column(name = "fecha_hora_registro",nullable = false)
  private Date fechaHoraRegistro;

}
