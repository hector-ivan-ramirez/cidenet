package col.humanos.cidenet.controller;

import col.humanos.cidenet.model.Empleado;
import col.humanos.cidenet.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping(value="/api")
public class EmpleadoController {

  private EmpleadoService empleadoService;

  @Autowired
  public EmpleadoController(EmpleadoService empleadoService) {
    this.empleadoService = empleadoService;
  }

  @GetMapping("empleados")
  public List<Empleado> todosEmpleados() {
    return empleadoService.busquedaAvanzada();
  }

  @PostMapping("crear-empleado")
  public boolean saveEmpleado(@RequestBody Empleado empleado) {
    empleado.setFechaHoraRegistro(new Date());
    return empleadoService.guardarEmpleado(empleado);
  }

  @DeleteMapping("borrar-empleado/{id}")
  public boolean deleteEmpleado(@PathVariable("id") int id, Empleado empleado) {
    empleado.setId(BigInteger.valueOf(id));
    return empleadoService.borrarEmpleado(empleado);
  }

  @GetMapping("empleado/{id}")
  public List<Empleado> allEmpleadoByID(@PathVariable("id") int id,Empleado empleado) {
    empleado.setId(BigInteger.valueOf(id));
    return empleadoService.getEmpleadoByID(empleado);

  }

  @PostMapping("actualizar-empleado/{id}")
  public boolean updateStudent(@RequestBody Empleado empleado,@PathVariable("id") int id) {
    empleado.setId(BigInteger.valueOf(id));
    return empleadoService.guardarEmpleado(empleado);
  }

}
